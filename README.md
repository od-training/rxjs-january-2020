# Oasis Digital RxJS Training

The main course materials are contained in **the `rxjs` directory**.

There are two additional directories that contain the code for servers that are used for some steps.

## Getting started

- Download the repo
- cd into the project directory
- `cd rxjs`
- `npm install` inside `/rxjs`
- `npm run 100` to start the first example app
- Visit [localhost:4200](http://localhost:4200) to view the app
- Find the code for the examples in the `/projects` directory
- Each step can be run with `npm run` plus the number from the directory name

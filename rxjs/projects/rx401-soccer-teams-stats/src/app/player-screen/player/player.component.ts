import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { pluck, switchMap, share, map } from 'rxjs/operators';
import { Observable, merge, Subject } from 'rxjs';

import { selectedPlayerIdRouteParamName, cardTypes } from '../../feature.constants';
import { PlayerService } from '../../player.service';

interface PlayerDataByGame {
  location: string;
  date: string;
  shots: number;
  goals: number;
  assists: number;
  redCard: boolean;
  yellowCard: boolean;
}

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  player = this.ar.params.pipe(
    pluck<{}, string>(selectedPlayerIdRouteParamName),
    switchMap(id => this.ps.playerWithStats(id)),
    share()
  );

  deleting = new Subject<string>();

  displayedColumns = ['gameName', 'location', 'date', 'goals', 'shots', 'shotAverage', 'assists', 'yellowCard', 'redCard'];

  playerGameTableData: Observable<PlayerDataByGame[]> = this.player.pipe(map(p =>
    p.games.map(g => ({
      name: g.name,
      location: g.location,
      date: g.date,
      shots: p.shotsOnGoal.filter(s => s.game === g.id).length,
      goals: p.shotsOnGoal.filter(s => s.game === g.id).filter(sog => sog.scored).length,
      assists: p.assists.filter(s => s.game === g.id).length,
      redCard: p.cards.filter(c => c.game === g.id && c.type === cardTypes.red).length > 0,
      yellowCard: p.cards.filter(c => c.game === g.id && c.type === cardTypes.yellow).length > 0
    }))
  ));

  canDelete = merge(
    this.ar.params.pipe(pluck<{}, string>(selectedPlayerIdRouteParamName), map(() => 'Loading player data')),
    this.playerGameTableData.pipe(map(td => td.length > 0 ? 'Cannot delete a player that has games' : undefined)),
    this.deleting
  );

  playerNameEdit = this.player.pipe(map(p => new FormControl(p.name)));

  constructor(private ar: ActivatedRoute, private ps: PlayerService, private router: Router) { }

  ngOnInit() {}

  updateName(newName: string) {
    this.ps.changePlayerName(
      this.ar.snapshot.params[selectedPlayerIdRouteParamName],
      newName
    );
  }

  delete() {
    this.deleting.next('Deletion in progress');
    this.ps.deletePlayer(this.ar.snapshot.params[selectedPlayerIdRouteParamName])
      .then(() => this.router.navigate(['soccer-team-stats', 'players']))
      .catch(() => this.deleting.next(undefined));
  }

}

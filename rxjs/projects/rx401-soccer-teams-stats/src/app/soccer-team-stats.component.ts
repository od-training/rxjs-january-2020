import { Component } from '@angular/core';

@Component({
  selector: 'app-soccer-team-stats',
  templateUrl: 'soccer-team-stats.component.html',
  styleUrls: ['./soccer-team-stats.component.css']
})
export class SoccerTeamStatsComponent {}

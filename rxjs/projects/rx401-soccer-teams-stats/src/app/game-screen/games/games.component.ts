import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { GameService } from '../../game.service';
import { AddGameComponent } from '../add-game/add-game.component';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  games = this.gs.games;

  constructor(private gs: GameService, private dialog: MatDialog) { }

  ngOnInit() {
  }

  addGame() {
    this.dialog.open(AddGameComponent);
  }

}

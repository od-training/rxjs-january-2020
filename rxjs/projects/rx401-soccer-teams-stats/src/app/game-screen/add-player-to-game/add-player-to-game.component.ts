import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Player } from '../../api-types';
import { PlayerService } from '../../player.service';
import { map } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { GameService } from '../../game.service';

export interface AddPlayerToGameData {
  gameId: string;
  existingPlayers: Player[];
}

@Component({
  selector: 'app-add-player-to-game',
  templateUrl: './add-player-to-game.component.html',
  styleUrls: ['./add-player-to-game.component.css']
})
export class AddPlayerToGameComponent implements OnInit {

  playerOptions = this.ps.players.pipe(map(players => players.filter(p => !this.data.existingPlayers.find(x => x.id === p.id))));
  chosenPlayer = new FormControl('', Validators.required);
  saving = false;

  constructor(
    private dialogRef: MatDialogRef<AddPlayerToGameComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddPlayerToGameData,
    private ps: PlayerService,
    private gs: GameService) { }

  ngOnInit() {
  }

  save() {
    this.saving = true;
    this.gs.addPlayerToGame(this.data.gameId, this.chosenPlayer.value)
    .then(() => this.dialogRef.close())
    .catch(e => {
      console.log(e);
      this.saving = false;
    });
  }

}

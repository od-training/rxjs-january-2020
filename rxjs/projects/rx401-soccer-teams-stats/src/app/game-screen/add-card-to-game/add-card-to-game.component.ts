import { Component, OnInit, Inject } from '@angular/core';
import { Player } from '../../api-types';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GameService } from '../../game.service';
import { cardTypes } from '../../feature.constants';

export interface AddCardToGameData {
  gameId: string;
  existingPlayers: Player[];
}

@Component({
  selector: 'app-add-card-to-game',
  templateUrl: './add-card-to-game.component.html',
  styleUrls: ['./add-card-to-game.component.css']
})
export class AddCardToGameComponent implements OnInit {
  cardForm = this.fb.group({
    player: ['', Validators.required],
    type: ['yellow'],
    minute: [0, Validators.required]
  });
  saving = false;
  cardtypes = Object.keys(cardTypes).map(k => cardTypes[k]);

  constructor(
      private dialogRef: MatDialogRef<AddCardToGameComponent>,
      @Inject(MAT_DIALOG_DATA) public data: AddCardToGameData,
      private gs: GameService,
      private fb: FormBuilder
    ) { }

  ngOnInit() {
  }

  save() {
    this.saving = true;
    this.gs.addCardToGame({
      game: this.data.gameId,
      ...this.cardForm.value
    })
    .then(() => this.dialogRef.close())
    .catch(e => {
      console.log(e);
      this.saving = false;
    });
  }
}

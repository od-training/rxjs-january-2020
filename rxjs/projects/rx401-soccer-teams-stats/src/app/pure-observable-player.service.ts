import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, switchMap, startWith, shareReplay} from 'rxjs/operators';
import { Observable, forkJoin, Subject } from 'rxjs';

import { playerEndpointLocation, gameEndpointLocation, goalEndpointLocation, cardEndpointLocation } from './api-urls';
import { Player, Game, ShotsOnGoal, Card, PlayerWithStats, ShotsOnGoalWithNames } from './api-types';
import { PlayerService } from './player.service';

// TODO: implement error handling

@Injectable()
export class PureObservablePlayerService extends PlayerService {

  private loadList = new Subject<void>();
  readonly players = this.loadList.pipe(
    startWith(undefined),
    switchMap(() => this.http.get<Player[]>(playerEndpointLocation)),
    shareReplay(1)
  );

  constructor(private http: HttpClient) {
    super();
  }

  player(id: string) {
    return this.http.get<Player>(`${playerEndpointLocation}/${id}`);
  }

  async addPlayer(name: string) {
    await this.http.post<Player>(playerEndpointLocation, { name }).toPromise();
    this.loadList.next();
  }

  async changePlayerName(id: string, newName: string) {
    await this.http.put<Player>(`${playerEndpointLocation}/${id}`, { id, name: newName }).toPromise();
    this.loadList.next();
  }

  playerGames(id: string) {
    return this.http
      .get<Game[]>(gameEndpointLocation)
      .pipe(map(games => games.filter(g => g.players.includes(id))));
  }

  playerShots(id: string): Observable<ShotsOnGoalWithNames[]> {
    return this.http
      .get<ShotsOnGoal[]>(`${goalEndpointLocation}?player=${id}`)
      .pipe(
        switchMap(shots =>
          forkJoin(
            shots.map(s =>
              forkJoin(this.player(s.player), this.player(s.assist)).pipe(
                map(([player, assist]) => ({
                  ...s,
                  playerName: player.name,
                  assistName: assist.name
                }))
              )
            )
          )
        ),
        startWith([])
      );
  }

  playerCards(id: string) {
    return this.http.get<Card[]>(`${cardEndpointLocation}?player=${id}`);
  }

  playerAssists(id: string): Observable<ShotsOnGoalWithNames[]> {
    return this.http
      .get<ShotsOnGoal[]>(`${goalEndpointLocation}?assist=${id}`)
      .pipe(
        switchMap(shots =>
          forkJoin(
            shots.map(s =>
              forkJoin(this.player(s.player), this.player(s.assist)).pipe(
                map(([player, assist]) => ({
                  ...s,
                  playerName: player.name,
                  assistName: assist.name
                }))
              )
            )
          )
        ),
        startWith([])
      );
  }

  playerWithStats(id: string): Observable<PlayerWithStats> {
    return forkJoin(
      this.player(id),
      this.playerGames(id),
      this.playerShots(id),
      this.playerCards(id),
      this.playerAssists(id)
    ).pipe(
      map(([player, games, shotsOnGoal, cards, assists]) => ({
        ...player,
        games,
        shotsOnGoal,
        cards,
        assists
      }))
    );
  }

  async deletePlayer(id: string) {
    await this.http.delete<void>(`${playerEndpointLocation}/${id}`).toPromise();
    this.loadList.next();
  }
}

export interface Player {
  id: string;
  name: string;
}

export interface Game {
  id: string;
  name: string;
  date: string;
  location: string;
  players: string[];
}

export interface ShotsOnGoal {
  id: string;
  player: string;
  game: string;
  assist: string;
  scored: boolean;
  minute: number;
}

export interface ShotsOnGoalWithNames extends ShotsOnGoal {
  playerName: string;
  assistName: string;
}

export interface Card {
  id: string;
  type: 'red' | 'yellow';
  game: string;
  player: string;
  minute: number;
}

export interface CardWithName extends Card {
  playerName: string;
}

export interface PlayerWithStats extends Player {
  games: Game[];
  shotsOnGoal: ShotsOnGoalWithNames[];
  cards: Card[];
  assists: ShotsOnGoalWithNames[];
}

export interface GameWithEvents extends Game {
  shots: ShotsOnGoalWithNames[];
  cards: CardWithName[];
  playerDetails: Player[];
}

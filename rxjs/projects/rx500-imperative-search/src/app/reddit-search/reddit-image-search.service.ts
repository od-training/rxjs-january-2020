import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { ImageMetadata, RedditResponse, RedditResponseChild } from '../types';

@Injectable({
  providedIn: 'root'
})
export class RedditImageSearchService {
  constructor(private http: HttpClient) {}

  search(subReddit: string, search: string): Promise<ImageMetadata[]> {
    const url = `https://www.reddit.com/r/${subReddit}/search.json`;
    const params = new HttpParams().set('restrict_sr', 'on').set('q', search);

    return this.http
      .get<RedditResponse>(url, { params })
      .pipe(
        delay(Math.random() * 5000), // Simulate flaky connection
        tap(() => console.log(`results for ${search}`))
      )
      .toPromise()
      .then(translateRedditResults);
  }
}

function translateRedditResults(items: RedditResponse): ImageMetadata[] {
  // This function doesn't know anything about HTTP or Observable; it just
  // manages the messy shape of this API's data return layout.

  return items.data.children
    .filter((item: RedditResponseChild) => {
      return !!item && !!item.data && item.data.thumbnail.startsWith('http');
    })
    .map(
      (item: RedditResponseChild): ImageMetadata => {
        const thumbnail = item.data.thumbnail;
        const title = item.data.title;
        return { thumbnail, title };
      }
    );
}

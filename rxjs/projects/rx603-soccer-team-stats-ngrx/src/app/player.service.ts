import { Observable } from 'rxjs';
import {
  Player,
  Game,
  ShotsOnGoalWithNames,
  Card,
  PlayerWithStats
} from './api-types';

export abstract class PlayerService {
  abstract players: Observable<Player[]>;

  abstract player(playerId: string): Observable<Player>;

  abstract async addPlayer(name: string): Promise<void>;

  abstract async changePlayerName(
    playerId: string,
    newName: string
  ): Promise<void>;

  abstract playerGames(playerId: string): Observable<Game[]>;

  abstract playerShots(playerId: string): Observable<ShotsOnGoalWithNames[]>;

  abstract playerCards(playerId: string): Observable<Card[]>;

  abstract playerAssists(playerId: string): Observable<ShotsOnGoalWithNames[]>;

  abstract playerWithStats(playerId: string): Observable<PlayerWithStats>;

  abstract async deletePlayer(playerId: string): Promise<void>;
}

import { Action } from '@ngrx/store';
import { Player, Game, Card, ShotsOnGoal } from '../api-types';
import {
  GET_PLAYERS_SUCCESS,
  GetPlayersSuccess,
  GetGamesSuccess,
  GET_GAMES_SUCCESS,
  GET_CARDS_SUCCESS,
  GetCardsSuccess,
  GET_SHOTS_SUCCESS,
  GetShotsSuccess,
  ADD_PLAYER,
  AddPlayer,
  DELETE_PLAYER,
  DeletePlayer,
  UPDATE_PLAYER_NAME,
  UpdatePlayerName
} from './actions';
import { initialState, soccerTeamFeatureSelector } from './state';
import { createEntityAdapter } from '@ngrx/entity';

const playerAdapter = createEntityAdapter<Player>();

export function playersReducer(prevState = initialState.players, a: Action) {
  switch (a.type) {
    case GET_PLAYERS_SUCCESS:
      return playerAdapter.addAll((a as GetPlayersSuccess).players, prevState);
    case ADD_PLAYER:
      return playerAdapter.addOne((a as AddPlayer).player, prevState);
    case DELETE_PLAYER:
      return playerAdapter.removeOne((a as DeletePlayer).id, prevState);
    case UPDATE_PLAYER_NAME:
      return playerAdapter.updateOne(
        {
          id: (a as UpdatePlayerName).id,
          changes: {
            name: (a as UpdatePlayerName).newName
          }
        },
        prevState
      );
    default:
      return prevState;
  }
}
export function gamesReducer(
  prevState: Game[] = initialState.games,
  a: Action
) {
  switch (a.type) {
    case GET_GAMES_SUCCESS:
      return (a as GetGamesSuccess).games;
    default:
      return prevState;
  }
}
export function cardsReducer(
  prevState: Card[] = initialState.cards,
  a: Action
) {
  switch (a.type) {
    case GET_CARDS_SUCCESS:
      return (a as GetCardsSuccess).cards;
    default:
      return prevState;
  }
}
export function shotsReducer(
  prevState: ShotsOnGoal[] = initialState.shots,
  a: Action
) {
  switch (a.type) {
    case GET_SHOTS_SUCCESS:
      return (a as GetShotsSuccess).shots;
    default:
      return prevState;
  }
}

// get the selectors
export const {
  selectIds: selectPlayerIds,
  selectAll: selectAllPlayers
} = playerAdapter.getSelectors();

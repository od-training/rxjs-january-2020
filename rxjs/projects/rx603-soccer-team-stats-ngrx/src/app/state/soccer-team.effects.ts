import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OnInitEffects, Actions, Effect, ofType } from '@ngrx/effects';
import {
  GetAll,
  GET_ALL,
  GetCards,
  GetPlayers,
  GetGames,
  GetShots,
  GET_CARDS,
  GetCardsSuccess,
  GetCardsFailure,
  GET_PLAYERS,
  GetPlayersSuccess,
  GetPlayersFailure,
  GET_GAMES,
  GetGamesSuccess,
  GetGamesFailure,
  GET_SHOTS,
  GetShotsSuccess,
  GetShotsFailure
} from './actions';
import { switchMap, mergeMap, map, catchError } from 'rxjs/operators';
import { of, from } from 'rxjs';
import {
  cardEndpointLocation,
  playerEndpointLocation,
  gameEndpointLocation,
  goalEndpointLocation
} from '../api-urls';
import { Card, Player, Game, ShotsOnGoal } from '../api-types';

@Injectable()
export class SoccerTeamEffects implements OnInitEffects {
  @Effect() translateGetAll = this.actions.pipe(
    ofType(GET_ALL),
    mergeMap(() => [
      new GetCards(),
      new GetPlayers(),
      new GetGames(),
      new GetShots()
    ])
  );
  @Effect() getCards = this.actions.pipe(
    ofType(GET_CARDS),
    switchMap(() =>
      this.http.get<Card[]>(cardEndpointLocation).pipe(
        map(cards => new GetCardsSuccess(cards)),
        catchError(e => of(new GetCardsFailure(e)))
      )
    )
  );
  @Effect() getPlayers = this.actions.pipe(
    ofType(GET_PLAYERS),
    switchMap(() =>
      this.http.get<Player[]>(playerEndpointLocation).pipe(
        map(players => new GetPlayersSuccess(players)),
        catchError(e => of(new GetPlayersFailure(e)))
      )
    )
  );
  @Effect() getGames = this.actions.pipe(
    ofType(GET_GAMES),
    switchMap(() =>
      this.http.get<Game[]>(gameEndpointLocation).pipe(
        map(Gamess => new GetGamesSuccess(Gamess)),
        catchError(e => of(new GetGamesFailure(e)))
      )
    )
  );
  @Effect() getShots = this.actions.pipe(
    ofType(GET_SHOTS),
    switchMap(() =>
      this.http.get<ShotsOnGoal[]>(goalEndpointLocation).pipe(
        map(Shots => new GetShotsSuccess(Shots)),
        catchError(e => of(new GetShotsFailure(e)))
      )
    )
  );
  constructor(private http: HttpClient, private actions: Actions) {}

  ngrxOnInitEffects() {
    return new GetAll();
  }
}

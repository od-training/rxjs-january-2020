import { Action } from '@ngrx/store';
import { Player, Game, Card, ShotsOnGoal } from '../api-types';
import { HttpErrorResponse } from '@angular/common/http';

export const GET_ALL = 'GET_ALL';
export class GetAll implements Action {
  type = GET_ALL;
}

export const GET_PLAYERS = 'GET_PLAYERS';
export class GetPlayers implements Action {
  type = GET_PLAYERS;
}
export const GET_GAMES = 'GET_GAMES';
export class GetGames implements Action {
  type = GET_GAMES;
}
export const GET_SHOTS = 'GET_SHOTS';
export class GetShots implements Action {
  type = GET_SHOTS;
}
export const GET_CARDS = 'GET_CARDS';
export class GetCards implements Action {
  type = GET_CARDS;
}
export const GET_PLAYERS_SUCCESS = 'GET_PLAYERS_SUCCESS';
export class GetPlayersSuccess implements Action {
  type = GET_PLAYERS_SUCCESS;
  constructor(readonly players: Player[]) {}
}
export const GET_GAMES_SUCCESS = 'GET_GAMES_SUCCESS';
export class GetGamesSuccess implements Action {
  type = GET_GAMES_SUCCESS;
  constructor(readonly games: Game[]) {}
}
export const GET_SHOTS_SUCCESS = 'GET_SHOTS_SUCCESS';
export class GetShotsSuccess implements Action {
  type = GET_SHOTS_SUCCESS;
  constructor(readonly shots: ShotsOnGoal[]) {}
}
export const GET_CARDS_SUCCESS = 'GET_CARDS_SUCCESS';
export class GetCardsSuccess implements Action {
  type = GET_CARDS_SUCCESS;
  constructor(readonly cards: Card[]) {}
}
export const GET_PLAYERS_FAILURE = 'GET_PLAYERS_FAILURE';
export class GetPlayersFailure implements Action {
  type = GET_PLAYERS_FAILURE;
  constructor(readonly error: HttpErrorResponse) {}
}
export const GET_GAMES_FAILURE = 'GET_GAMES_FAILURE';
export class GetGamesFailure implements Action {
  type = GET_GAMES_FAILURE;
  constructor(readonly error: HttpErrorResponse) {}
}
export const GET_SHOTS_FAILURE = 'GET_SHOTS_FAILURE';
export class GetShotsFailure implements Action {
  type = GET_SHOTS_FAILURE;
  constructor(readonly error: HttpErrorResponse) {}
}
export const GET_CARDS_FAILURE = 'GET_CARDS_FAILURE';
export class GetCardsFailure implements Action {
  type = GET_CARDS_FAILURE;
  constructor(readonly error: HttpErrorResponse) {}
}

export const ADD_PLAYER = 'ADD_PLAYER';
export class AddPlayer implements Action {
  type = ADD_PLAYER;
  constructor(readonly player: Player) {}
}

export const DELETE_PLAYER = 'DELETE_PLAYER';
export class DeletePlayer implements Action {
  type = DELETE_PLAYER;
  constructor(readonly id: string) {}
}

export const UPDATE_PLAYER_NAME = 'UPDATE_PLAYER_NAME';
export class UpdatePlayerName implements Action {
  type = UPDATE_PLAYER_NAME;
  constructor(readonly id: string, readonly newName: string) {}
}

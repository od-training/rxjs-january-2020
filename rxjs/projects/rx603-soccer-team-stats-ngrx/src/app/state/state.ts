import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { Game, Player, ShotsOnGoal, Card } from '../api-types';
import { createFeatureSelector } from '@ngrx/store';

export interface SoccerTeamState {
  games: Game[];
  players: EntityState<Player>;
  shots: ShotsOnGoal[];
  cards: Card[];
}

export const playerAdapter = createEntityAdapter<Player>();

export const initialState: SoccerTeamState = {
  games: [],
  players: playerAdapter.getInitialState(),
  shots: [],
  cards: []
};

export const soccerTeamProperty = 'soccerTeam';
export const soccerTeamFeatureSelector = createFeatureSelector<SoccerTeamState>(
  soccerTeamProperty
);

export const playerEndpointLocation = 'api/players';
export const gameEndpointLocation = 'api/games';
export const goalEndpointLocation = 'api/shotsongoal';
export const cardEndpointLocation = 'api/cards';

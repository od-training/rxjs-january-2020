import { PlayerService } from './player.service';
import { Store, select } from '@ngrx/store';
import { SoccerTeamState, soccerTeamFeatureSelector } from './state/state';
import { HttpClient } from '@angular/common/http';
import { playerEndpointLocation } from './api-urls';
import { AddPlayer, DeletePlayer, UpdatePlayerName } from './state/actions';
import { Player, PlayerWithStats } from './api-types';
import { combineLatest, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { selectAllPlayers } from './state/reducers';

export const getAssistsForPlayer = (
  s: { soccerTeam: SoccerTeamState },
  playerId: string
) =>
  s.soccerTeam.shots
    .filter(shot => shot.assist === playerId)
    .map(shot => {
      const player = s.soccerTeam.players.entities[shot.player];
      const assist = s.soccerTeam.players.entities[shot.assist];
      const playerName = player ? player.name : '';
      const assistName = assist ? assist.name : '';
      return {
        ...shot,
        playerName,
        assistName
      };
    });

const check = (p: Player | undefined): p is Player => !!p;

@Injectable()
export class NgrxPlayerService extends PlayerService {
  players = this.store.pipe(
    select(soccerTeamFeatureSelector),
    select(s => s.players),
    select(selectAllPlayers)
  );
  player(playerId: string): Observable<Player> {
    return this.store.pipe(
      select(soccerTeamFeatureSelector),
      select(s => s.players.entities[playerId]),
      filter(check)
    );
  }

  constructor(
    private store: Store<{ soccerTeam: SoccerTeamState }>,
    private http: HttpClient
  ) {
    super();
  }

  async addPlayer(name: string): Promise<void> {
    await this.http
      .post<Player>(playerEndpointLocation, {
        name
      })
      .toPromise()
      .then(p => this.store.dispatch(new AddPlayer(p)));
  }
  async changePlayerName(id: string, name: string): Promise<void> {
    await this.http
      .put(`${playerEndpointLocation}/${id}`, { id, name })
      .toPromise()
      .then(() => this.store.dispatch(new UpdatePlayerName(id, name)));
  }
  playerGames(playerId: string) {
    return this.store.pipe(
      select(s => s.soccerTeam.games.filter(g => g.players.includes(playerId)))
    );
  }
  playerShots(playerId: string) {
    return this.store.pipe(
      select(s =>
        s.soccerTeam.shots
          .filter(shot => shot.player === playerId)
          .map(shot => {
            const player = s.soccerTeam.players.entities[shot.player];
            const assist = s.soccerTeam.players.entities[shot.assist];
            const playerName = player ? player.name : '';
            const assistName = assist ? assist.name : '';
            return {
              ...shot,
              playerName,
              assistName
            };
          })
      )
    );
  }
  playerCards(playerId: string) {
    return this.store.pipe(
      select(s => s.soccerTeam.cards.filter(c => c.player === playerId))
    );
  }
  playerAssists(playerId: string) {
    return this.store.pipe(select(s => getAssistsForPlayer(s, playerId)));
  }
  playerWithStats(id: string): Observable<PlayerWithStats> {
    return combineLatest(
      this.player(id),
      this.playerGames(id),
      this.playerShots(id),
      this.playerCards(id),
      this.playerAssists(id)
    ).pipe(
      map(([player, games, shotsOnGoal, cards, assists]) => ({
        ...player,
        games,
        shotsOnGoal,
        cards,
        assists
      }))
    );
  }
  async deletePlayer(id: string): Promise<void> {
    await this.http
      .delete<void>(`${playerEndpointLocation}/${id}`)
      .toPromise()
      .then(() => this.store.dispatch(new DeletePlayer(id)));
  }
}

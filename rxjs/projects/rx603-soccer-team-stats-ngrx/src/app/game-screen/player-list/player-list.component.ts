import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Player } from '../../api-types';

import {
  AddPlayerToGameComponent,
  AddPlayerToGameData
} from '../add-player-to-game/add-player-to-game.component';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {
  @Input() players!: Player[];
  @Input() gameId!: string;
  @Output() playerListUpdated = new EventEmitter<void>();
  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  addPlayer() {
    const data: AddPlayerToGameData = {
      gameId: this.gameId,
      existingPlayers: this.players
    };
    this.dialog.open(AddPlayerToGameComponent, { data }).afterClosed();
  }
}

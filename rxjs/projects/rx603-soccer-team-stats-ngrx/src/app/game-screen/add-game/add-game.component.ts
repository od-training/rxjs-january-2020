import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { GameService } from '../../game.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent {
  saving = false;
  locationInput = new FormControl('', Validators.required);
  dateInput = new FormControl('', Validators.required);
  nameInput = new FormControl('', Validators.required);

  constructor(private dialogRef: MatDialogRef<AddGameComponent>, private gs: GameService) { }

  save() {
    this.saving = true;
    this.gs.addGame(
      this.locationInput.value,
      this.dateInput.value.format('YYYY-MM-DD'),
      this.nameInput.value)
      .then(() => this.dialogRef.close())
      .catch(e => {
        console.log(e);
        this.saving = false;
      });
  }

}

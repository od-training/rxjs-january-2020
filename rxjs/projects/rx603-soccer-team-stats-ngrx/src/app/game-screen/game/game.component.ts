import { Component, OnInit } from '@angular/core';
import { GameService } from '../../game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, share } from 'rxjs/operators';
import { selectedGameIdRouteParamName } from '../../feature.constants';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  gameId = this.ar.params.pipe(map(p => p[selectedGameIdRouteParamName]));
  game = this.gameId.pipe(
    switchMap(id => this.gs.getGameWithDetails(id)),
    share()
  );
  deleting = false;

  constructor(
    private gs: GameService,
    private ar: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}

  delete() {
    this.deleting = true;
    this.gs
      .deleteGame(this.ar.snapshot.params[selectedGameIdRouteParamName])
      .then(() => this.router.navigate(['soccer-team-stats', 'games']))
      .catch(() => (this.deleting = false));
  }
}

import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { BehaviorSubject } from 'rxjs';
import { SoccerTeamState, initialState } from './state/state';
import { Action, Store } from '@ngrx/store';
import { NgrxPlayerService, getAssistsForPlayer } from './ngrx-player.service';
import { createEntityAdapter } from '@ngrx/entity';
import { Player } from './api-types';
import { ADD_PLAYER, AddPlayer } from './state/actions';
import { playerEndpointLocation } from './api-urls';

class MockStore {
  state = new BehaviorSubject<{ soccerTeam: SoccerTeamState }>({
    soccerTeam: initialState
  });
  actions = [];
  pipe(...args) {
    return this.state.pipe(...args);
  }
  dispatch(a: Action) {
    this.actions.push(a);
  }
}

const makeFakePlayer = (): Player => ({
  name: `Test User #${Math.floor(Math.random() * 1000)}`,
  id: `${Math.floor(Math.random() * 1000)}`
});

fdescribe('The player service based on ngrx', () => {
  let service: NgrxPlayerService;
  let store: MockStore;
  let http: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: Store,
          useClass: MockStore
        },
        NgrxPlayerService
      ]
    });
    service = TestBed.get(NgrxPlayerService);
    store = TestBed.get(Store);
    http = TestBed.get(HttpTestingController);
  });

  it('provides a list of players', () => {
    service.players.subscribe(players => expect(players).toEqual([]));
  });
  it('gets individual players for you', () => {
    const playerAdapter = createEntityAdapter<Player>();
    const state = playerAdapter.getInitialState();
    const testPlayer = {
      id: 'def',
      name: 'Kyle Cordes'
    };
    const newState = playerAdapter.addAll(
      [
        testPlayer,
        {
          id: 'abc',
          name: 'Jack Balbes'
        }
      ],
      state
    );
    store.state.next({
      soccerTeam: {
        cards: [],
        games: [],
        shots: [],
        players: newState
      }
    });
    service.player('def').subscribe(player => expect(player).toBe(testPlayer));
  });
  it('should add a player', () => {
    const fakePlayer = makeFakePlayer();

    service.addPlayer(fakePlayer.name).then(() => {
      expect(store.actions.length).toBe(1);
      expect(store.actions[0].type).toBe(ADD_PLAYER);
      expect((store.actions[0] as AddPlayer).player).toEqual(fakePlayer);
    });

    const req = http.expectOne(playerEndpointLocation);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      name: fakePlayer.name
    });
    req.flush(fakePlayer);
  });

  it('should get a list of shots populated with player names', () => {
    const state: { soccerTeam: SoccerTeamState } = {
      soccerTeam: {
        cards: [],
        games: [],
        players: {
          entities: {
            def: {
              id: 'def',
              name: 'Test User'
            },
            abc: {
              id: 'abc',
              name: 'Test Assist'
            }
          },
          ids: ['def', 'abc']
        },
        shots: [
          {
            id: 'fds',
            assist: 'abc',
            game: 'dasda',
            minute: 3,
            player: 'def',
            scored: true
          }
        ]
      }
    };
    const result = getAssistsForPlayer(state, 'abc');
    expect(result.length).toBe(1);
    expect(result[0].id).toBe('fds');
    expect(result[0].playerName).toBe('Test User');
    expect(result[0].assistName).toBe('Test Assist');
  });
});

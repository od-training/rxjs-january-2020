import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game, ShotsOnGoal, Card, GameWithEvents } from './api-types';
import {
  gameEndpointLocation,
  goalEndpointLocation,
  cardEndpointLocation
} from './api-urls';
import { switchMap, map, shareReplay } from 'rxjs/operators';
import { forkJoin, Observable, BehaviorSubject, of } from 'rxjs';
import { PlayerService } from './player.service';

@Injectable()
export class GameService {
  refreshGameData = new BehaviorSubject<void>(undefined);
  readonly games = this.refreshGameData
    .pipe(switchMap(() => this.http.get<Game[]>(gameEndpointLocation)))
    .pipe(shareReplay(1));
  constructor(private http: HttpClient, private ps: PlayerService) {}

  getGame(id: string) {
    return this.http.get<Game>(`${gameEndpointLocation}/${id}`);
  }

  getShotsForGame(id: string) {
    return this.http.get<ShotsOnGoal[]>(`${goalEndpointLocation}?game=${id}`);
  }

  getCardsForGame(id: string) {
    return this.http.get<Card[]>(`${cardEndpointLocation}?game=${id}`);
  }

  getPlayerDetails(ids: string[]) {
    // return forkJoin(ids.map(id => this.ps.player(id)));
    return ids.length > 0
      ? forkJoin(ids.map(id => this.ps.player(id)))
      : of([]);
  }

  getGameWithDetails(id: string): Observable<GameWithEvents> {
    return this.games
      .pipe(
        switchMap(games => {
          const game = games.find(g => g.id === id);
          return forkJoin(
            this.getPlayerDetails(game ? game.players : []).pipe(
              map(playerDetails => ({ ...game, playerDetails }))
            ),
            this.getShotsForGame(id),
            this.getCardsForGame(id)
          );
        })
      )
      .pipe(
        map(([game, shots, cards]) => {
          const shotsWithNames = shots.map(s => ({
            ...s,
            playerName: game.playerDetails.find(p => p.id === s.player)!.name,
            assistName: s.assist
              ? game.playerDetails.find(p => p.id === s.assist)!.name
              : 'None'
          }));
          const cardsWithNames = cards.map(c => ({
            ...c,
            playerName: game.playerDetails.find(p => p.id === c.player)!.name
          }));
          return {
            ...game,
            shots: shotsWithNames,
            cards: cardsWithNames
          } as GameWithEvents;
        })
      );
  }

  async addGame(location: string, date: string, name: string) {
    await this.http
      .post<Game>(gameEndpointLocation, {
        name,
        date,
        location,
        players: []
      })
      .toPromise();
    this.refreshGameData.next();
  }

  async addPlayerToGame(gameId: string, playerId: string) {
    await this.getGame(gameId)
      .pipe(
        switchMap(game =>
          this.http.put(`${gameEndpointLocation}/${gameId}`, {
            ...game,
            players: [...game.players, playerId]
          })
        )
      )
      .toPromise();
    this.refreshGameData.next();
  }

  async addShotToGame(shot: ShotsOnGoal) {
    await this.http.post(goalEndpointLocation, shot).toPromise();
    this.refreshGameData.next();
  }

  async deleteGame(id: string) {
    await this.http.delete(`${gameEndpointLocation}/${id}`).toPromise();
    this.refreshGameData.next();
  }

  async addCardToGame(card: Card) {
    await this.http.post(cardEndpointLocation, card).toPromise();
    this.refreshGameData.next();
  }
}

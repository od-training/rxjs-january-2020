import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AddPlayerComponent } from './player-screen/add-player/add-player.component';
import { AddGameComponent } from './game-screen/add-game/add-game.component';
import { GamesComponent } from './game-screen/games/games.component';
import { PlayersComponent } from './player-screen/players/players.component';
import { SoccerTeamStatsComponent } from './soccer-team-stats.component';
import { PlayerComponent } from './player-screen/player/player.component';
import {
  selectedPlayerIdRouteParamName,
  selectedGameIdRouteParamName
} from './feature.constants';
import { ReactiveFormsModule } from '@angular/forms';
import { GameService } from './game.service';
import { GameComponent } from './game-screen/game/game.component';
import { PlayerService } from './player.service';
import { PlayerNameEditorComponent } from './player-screen/player-name-editor/player-name-editor.component';
import { PlayerListComponent } from './game-screen/player-list/player-list.component';
import { AddPlayerToGameComponent } from './game-screen/add-player-to-game/add-player-to-game.component';
import { ShotListComponent } from './game-screen/shot-list/shot-list.component';
import { CardListComponent } from './game-screen/card-list/card-list.component';
import { AddCardToGameComponent } from './game-screen/add-card-to-game/add-card-to-game.component';
import { AddShotToGameComponent } from './game-screen/add-shot-to-game/add-shot-to-game.component';
import { StoreModule } from '@ngrx/store';
import { SoccerTeamState, soccerTeamProperty } from './state/state';
import {
  cardsReducer,
  gamesReducer,
  playersReducer,
  shotsReducer
} from './state/reducers';
import { EffectsModule } from '@ngrx/effects';
import { SoccerTeamEffects } from './state/soccer-team.effects';
import { NgrxPlayerService } from './ngrx-player.service';
import {
  MatInputModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatCardModule,
  MatIconModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatButtonModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

const routes: Routes = [
  {
    path: '',
    component: SoccerTeamStatsComponent,
    children: [
      {
        path: 'players',
        component: PlayersComponent,
        children: [
          {
            path: `:${selectedPlayerIdRouteParamName}`,
            component: PlayerComponent
          }
        ]
      },
      {
        path: 'games',
        component: GamesComponent,
        children: [
          { path: `:${selectedGameIdRouteParamName}`, component: GameComponent }
        ]
      },
      { path: '', redirectTo: 'players' }
    ]
  }
];

@NgModule({
  declarations: [
    AddPlayerComponent,
    AddGameComponent,
    GamesComponent,
    PlayersComponent,
    SoccerTeamStatsComponent,
    PlayerComponent,
    GameComponent,
    PlayerNameEditorComponent,
    PlayerListComponent,
    AddPlayerToGameComponent,
    ShotListComponent,
    CardListComponent,
    AddCardToGameComponent,
    AddShotToGameComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    StoreModule.forFeature<SoccerTeamState>(soccerTeamProperty, {
      cards: cardsReducer,
      games: gamesReducer,
      players: playersReducer,
      shots: shotsReducer
    }),
    EffectsModule.forFeature([SoccerTeamEffects]),
    MatInputModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
    MatTableModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    {
      provide: PlayerService,
      useClass: NgrxPlayerService
    },
    GameService
  ],
  entryComponents: [
    AddPlayerComponent,
    AddGameComponent,
    AddPlayerToGameComponent,
    AddCardToGameComponent,
    AddShotToGameComponent
  ]
})
export class SoccerTeamStatsModule {}

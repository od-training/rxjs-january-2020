import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TemperatureModule } from './temperature/temperature.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, TemperatureModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

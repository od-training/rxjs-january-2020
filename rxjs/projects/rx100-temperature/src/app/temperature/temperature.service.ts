import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { map, tap, scan, take } from 'rxjs/operators';

const changeRange = 10;

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {
  temperatureHistory: Observable<number[]>;
  private temperatureChange: Observable<number>;
  private temperature: Observable<number>;

  constructor() {
    // Generate a series of changes
    this.temperatureChange = interval(100).pipe(
      // Data will probably trend upward
      map(() => Math.floor(Math.random() * changeRange) - changeRange / 2 + 1),
      tap(change => console.log('change', change))
    );

    // Calculate temp based on previous temp and change
    this.temperature = this.temperatureChange.pipe(
      take(100),
      scan((change, previous) => change + previous, 0),
      tap(temperature => console.log('temp', temperature))
    );

    // Calculate history based on previous history and new value
    this.temperatureHistory = this.temperature.pipe(
      scan<number>((history, newTemp) => [...history, newTemp], [])
    );
  }
}

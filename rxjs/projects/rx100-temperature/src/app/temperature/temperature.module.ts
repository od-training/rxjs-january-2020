import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LineGraphComponent } from './line-graph/line-graph.component';
import { TemperatureGraphComponent } from './temperature-graph/temperature-graph.component';

@NgModule({
  declarations: [LineGraphComponent, TemperatureGraphComponent],
  exports: [TemperatureGraphComponent],
  imports: [CommonModule]
})
export class TemperatureModule {}

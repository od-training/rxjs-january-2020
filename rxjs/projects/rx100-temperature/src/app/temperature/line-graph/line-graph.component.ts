import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

interface Segment {
  start: number;
  end: number;
}

@Component({
  selector: 'app-line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LineGraphComponent {
  @Input() set graphData(dataPoints: number[]) {
    if (dataPoints) {
      // Invert data since positive y is down
      this.points = dataPoints.map(i => -1 * i);
      this.lines = this.points
        .map((v, i) => ({ start: v, end: this.points[i + 1] }))
        .slice(0, this.points.length - 1);
    }
  }
  points: number[] = [];
  lines: Segment[] = [];
  height = 200;
  width = 1000;
  intervalSpacing = 10;
}

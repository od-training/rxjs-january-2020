"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const express = require("express");
const channels_1 = require("./channels");
const PORT = process.env.PORT || 8005;
const app = express();
// Serve static files for simple plain-JS demo
app.use(express.static(path.join(__dirname, '../www')));
app.get('/lowfreq', function (req, res) {
    channels_1.lowfreqChannel.addClient(req, res);
});
app.get('/highfreq', function (req, res) {
    channels_1.highfreqChannel.addClient(req, res);
});
app.listen(PORT, function () {
    console.log('Listening on http://localhost:%s/', PORT);
});
//# sourceMappingURL=main.js.map
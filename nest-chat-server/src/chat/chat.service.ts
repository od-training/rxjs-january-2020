import {WebSocketGateway, SubscribeMessage, WebSocketServer} from '@nestjs/websockets';

@WebSocketGateway()
export class ChatService {
  @WebSocketServer() server: any;
  private messages: string[] = [];

  @SubscribeMessage('chat')
  onChat(client: any, message: string) {
    this.messages.push(message);
    this.server.emit('chat', this.messages);
  }
}
